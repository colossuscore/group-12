#include "Engine.h"
#include "IState.h"
#include "GameState.h"
#include "DrawManager.h"


const int SCREENWIDTH = 1280;
const int SCREEHEIGHT = 720;

Engine::Engine()
{
	m_bRunning = false;
	m_pxStateManager = nullptr;
	m_pxDrawManager = nullptr;
}
Engine::~Engine()
{

}

bool Engine::Initialize()
{
	m_Window.create(sf::VideoMode(SCREENWIDTH, SCREEHEIGHT), "Colossus Core");

	

	m_pxStateManager = new StateManager();

	System system;
	system.m_iScreenWidth = SCREENWIDTH;
	system.m_iScreenHeight = SCREEHEIGHT;

	m_pxStateManager->SetState(new GameState(system));

	return true;
}

void Engine::Shutdown()
{

}

void Engine::Update()
{
	while (m_Window.isOpen())
	{
		while (m_bRunning)
		{
			

		}
		sf::Event event;
		while (m_Window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				m_Window.close();
			}
		}
		m_Window.clear(sf::Color(0x11, 0x22, 0x44, 0xff));
		m_Window.display();
	}
}

