#pragma once
#include "SFML.h"

class IEntity;

class Collider
{
public:
	Collider(float p_iWidth, float p_iHeight);
	void SetPosition(float p_iX, float p_iY);
	void SetSize(float p_iWidth, float p_iHeight);
	float GetX();
	float GetY();
	float GetW();
	float GetH();
	void SetParent(IEntity* p_pxParent);
	void Refresh();
private:
	Collider() {};
	IEntity* m_pxParent;
	sf::FloatRect m_xfRegion;
};