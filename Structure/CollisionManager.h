#pragma once

class Collider;

class CollisionManager
{
public:
	static bool Check(Collider* p_pxLeft, Collider* p_pxRight, float& p_iOverlapX, float& p_iOverlapY);
};