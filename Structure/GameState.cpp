#include "GameState.h"
#include "Player.h"
#include "SFML.h"

GameState::GameState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_pxPlayer = nullptr;
}

GameState::~GameState()
{
}

void GameState::Enter()
{
	m_pxPlayer = new Player();
	m_pxPlayer->SetPosition(300, 300);
}

void GameState::Exit()
{
}

bool GameState::Update()
{

	return true;
}

void GameState::Draw()
{
}

IState* GameState::NextState()
{
	return false;
}

void GameState::CheckCollision()
{
}
