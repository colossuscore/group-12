#pragma once
#include "SFML.h"
class Sprite;

class DrawManager
{
public:
	DrawManager();
	~DrawManager();
	bool Initialize(int p_iWidth, int p_iHeight);
	void Shutdown();
	void Clear();
	void Present();
	void Draw(Sprite* p_pxSprite, int p_iX, int p_iY);
	void DebugDraw(int p_iX, int p_iY, int p_iW, int p_iH);
	sf::RenderTexture* GetRenderer();
private:
	sf::RenderWindow m_pxWindow;
	sf::RenderTexture* m_pxRenderer = nullptr;
};