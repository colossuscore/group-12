#include "StateManager.h"
#include "IState.h"

StateManager::StateManager()
{
	m_pxCurrentState = nullptr;
}

StateManager::~StateManager()
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
}

bool StateManager::Update()
{
	if (m_pxCurrentState != nullptr)
	{
		if (m_pxCurrentState->Update() == false)
		{
			SetState(m_pxCurrentState->NextState());
		}
	}

	return true;
}

void StateManager::Draw()
{
	if(m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Draw();
	}
}

void StateManager::SetState(IState* p_pxState)
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
	
	m_pxCurrentState = p_pxState;
	m_pxCurrentState->Enter();
}