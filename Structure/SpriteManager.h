#pragma once
#pragma once

#ifndef Spritemanager_H
#define Spritemanager_H
#endif

#include <SFML/Graphics.hpp>
#include <map>
#include <string>


using namespace std;

class Spritemanager
{
private:
	static bool instanceFlag;
	static Spritemanager *single;
	std::map <string, sf::Image*> m_Images;
	sf::Image *m_Temp;
	Spritemanager()
	{

	}
public:
	static Spritemanager* getInstance();
	void destroy();
	void init();
	sf::Image* loadImage(string filename);
	~Spritemanager()
	{

	}
};