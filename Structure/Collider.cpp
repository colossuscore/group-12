#include "Collider.h"
#include "IEntity.h"

Collider::Collider(float p_iWidth, float p_iHeight)
{
	m_xfRegion.left = 0;
	m_xfRegion.top = 0;
	m_xfRegion.width = p_iWidth;
	m_xfRegion.height = p_iHeight;
	m_pxParent = nullptr;
}

void Collider::SetPosition(float p_iX, float p_iY)
{
	m_xfRegion.left = p_iX;
	m_xfRegion.top = p_iY;
}

void Collider::SetSize(float p_iWidth, float p_iHeight)
{
	if (p_iWidth < 0)
		p_iWidth = 0;
	if (p_iHeight < 0)
		p_iHeight = 0;
	m_xfRegion.width = p_iWidth;
	m_xfRegion.height = p_iHeight;
}

float Collider::GetX()
{
	return m_xfRegion.left;
};

float Collider::GetY()
{
	return m_xfRegion.top;
}

float Collider::GetW()
{
	return m_xfRegion.width;
}

float Collider::GetH()
{
	return m_xfRegion.height;
}

void Collider::SetParent(IEntity* p_pxParent)
{
	m_pxParent = p_pxParent;
}

void Collider::Refresh()
{
	if (m_pxParent == nullptr)
		return;
	m_xfRegion.left = m_pxParent->GetX();
	m_xfRegion.top = m_pxParent->GetY();
}