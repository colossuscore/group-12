#pragma once

class Player
{
public:
	Player(sf::Vector2f p_xPposition);
	void Update();
	void Movement(sf::Sprite &p_pxPlayer, sf::Vector2f &p_xsource);
	sf::Sprite playerImage;
	sf::Texture pTexture;
private:
	
	float bottom, left, right, top;
};