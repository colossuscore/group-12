#pragma once

class Player;

class EnemyStinger
{
public:
	EnemyStinger(sf::Vector2f p_xEStingerposition, float rotation);
	void Update();
	void Movement(Player player);
	sf::Sprite enemyImage;
	sf::Texture eTexture;
private:
	
	float ebottom, eleft, eright, etop;
};