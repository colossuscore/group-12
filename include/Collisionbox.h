#pragma once
class Player;
class EnemyStinger;
class Projectiles;

class CollisionBoundingBox
{
public:
	bool Collision(Player p_pxpPlayerCollider, EnemyStinger p_pxStingerCollider);
	bool Collision(Projectiles p_pxProjectilesCollider, EnemyStinger p_pxStingerCollider)
};