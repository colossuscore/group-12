#pragma once


class Projectile
{
public:
	Projectile(sf::Vector2f position, float rotation);
	void Move();
	void Update();
	sf::Sprite ProjectileImage;
	sf::Texture ProjectileTexture;
private:
	float bottom, left, right, top;
};