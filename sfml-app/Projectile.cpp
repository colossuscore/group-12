#include "SFML.h"
#include "Projectile.h"

Projectile::Projectile(sf::Vector2f position, float rotation)
{
	ProjectileImage.setPosition(position);
	if (!ProjectileTexture.loadFromFile("assets/Projectile.png"))
		std::cout << "Error could not load image" << std::endl;
	ProjectileImage.setTexture(ProjectileTexture);//305,441
	ProjectileImage.setOrigin(ProjectileImage.getLocalBounds().width, 0);
	ProjectileImage.setScale(0.7, 0.7);
	ProjectileImage.setRotation(rotation);
}

void Projectile::Move()
{
	float movementSpeed = 3.0f;
	ProjectileImage.move(cos(ProjectileImage.getRotation() * 3.14159 / 180)*(movementSpeed), sin(ProjectileImage.getRotation() * 3.14159 / 180)*(movementSpeed));

}

void Projectile::Update()
{
	bottom = ProjectileImage.getPosition().y + ProjectileImage.getLocalBounds().height;
	left = ProjectileImage.getPosition().x;
	right = ProjectileImage.getPosition().x + ProjectileImage.getLocalBounds().width;
	top = ProjectileImage.getPosition().y;
}