#include "SFML.h"
#include "Player.h"


Player::Player(sf::Vector2f p_xPposition)
{
	playerImage.setPosition(p_xPposition);
	if (!pTexture.loadFromFile("assets/Aquila.png"))
		std::cout << "Error could not load image" << std::endl;
	playerImage.setTexture(pTexture);//305,441
	playerImage.setOrigin(playerImage.getLocalBounds().width / 2, playerImage.getLocalBounds().height / 2);
	playerImage.setScale(1, 1);
}
void Player::Update()
{
	bottom = playerImage.getPosition().y + playerImage.getLocalBounds().height;
	left = playerImage.getPosition().x;
	right = playerImage.getPosition().x + playerImage.getLocalBounds().width;
	top = playerImage.getPosition().y;
}

void Player::Movement(sf::Sprite &p_pxPlayer, sf::Vector2f &p_xsource)
{
	/*PlayerMovement*/ {
		sf::Clock clockmovement;
		clockmovement.restart();
		float moveSpeed = 300.0f;
		float RotationSpeed = 0.15f;
		//Convert angle to radians
		float angleRADS;
		angleRADS = (3.1415926536 / 180)*(playerImage.getRotation());

		//Set x and y
		float forx = 0.01f*cos(angleRADS);
		float fory = 0.01f*sin(angleRADS);
		float olddir;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			p_pxPlayer.rotate(-RotationSpeed);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			p_pxPlayer.rotate(RotationSpeed);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{

			p_pxPlayer.move(forx  * moveSpeed, fory *  moveSpeed);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			p_pxPlayer.move(forx * -moveSpeed, fory *  -moveSpeed);
		}


	}
}
