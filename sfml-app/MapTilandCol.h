//void LoadMap(char* filename, std::vector<std::vector<sf::Vector2i> > &map, std::vector<sf::Vector2i> &tempMap, sf::Sprite &tiles)
//{
//	/*TileSystem*/
//	std::ifstream openfile(filename);
//	sf::Texture tileTexture;
//	
//
//	if (openfile.is_open())
//	{
//		std::string tileLocation;
//		openfile >> tileLocation;
//		tileTexture.loadFromFile(tileLocation);
//		tiles.setTexture(tileTexture);
//		while (!openfile.eof())
//		{
//
//			std::string str, value;
//			std::getline(openfile, str);
//			std::stringstream stream(str);
//
//			while (std::getline(stream, value, ' '))
//			{
//				if (value.length() > 0)
//				{
//					std::string xx = value.substr(0, value.find(','));
//					std::string yy = value.substr(value.find(',') + 1);
//
//					int x, y, i, j;
//					for (i = 0; i < xx.length(); i++)
//					{
//						if (!isdigit(xx[i]))
//							break;
//					}
//					for (j = 0; j < xx.length(); j++)
//					{
//						if (!isdigit(xx[j]))
//							break;
//					}
//					x = (j == xx.length()) ? atoi(xx.c_str()) : -1;
//					y = (j == yy.length()) ? atoi(yy.c_str()) : -1;
//
//					tempMap.push_back(sf::Vector2i(x, y));
//				}
//			}
//			if (tempMap.size() > 0)
//			{
//				map.push_back(tempMap);
//				tempMap.clear();
//			}
//		}
//	}
//}
//
//void LoadColMap(char* filename, std::vector<std::vector<int>> &colMap, std::vector<int> &col)
//{
//	/*TileSystem*/
//	std::ifstream openfile(filename);
//	sf::Texture tileTexture;
//	colMap.clear();
//
//	if (openfile.is_open())
//	{
//		std::string tileLocation;
//		openfile >> tileLocation;
//		tileTexture.loadFromFile(tileLocation);
//		while (!openfile.eof())
//		{
//
//			std::string str, value;
//			std::getline(openfile, str);
//			std::stringstream stream(str);
//
//			while (std::getline(stream, value, ' '))
//			{
//				if (value.length() > 0)
//				{
//					int a = atoi(value.c_str());
//					col.push_back(a);
//				}
//			}
//			colMap.push_back(col);
//			col.clear();
//		}
//	}
//}
