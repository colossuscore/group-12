#include "SFML.h"
#include "EnemyStinger.h"
#include "Player.h"

EnemyStinger::EnemyStinger(sf::Vector2f p_xEposition, float rotation)
{
	enemyImage.setPosition(p_xEposition);
	if (!eTexture.loadFromFile("assets/Stinger.png"))
		std::cout << "Error could not load image" << std::endl;
	enemyImage.setTexture(eTexture);//305,441
	enemyImage.setOrigin(enemyImage.getLocalBounds().width / 2, enemyImage.getLocalBounds().height / 2);
	enemyImage.setRotation(rotation);
}

void EnemyStinger::Update()
{
	ebottom = enemyImage.getPosition().y + enemyImage.getLocalBounds().height;
	eleft = enemyImage.getPosition().x;
	eright = enemyImage.getPosition().x + enemyImage.getLocalBounds().width;
	etop = enemyImage.getPosition().y;
}

void EnemyStinger::Movement(Player player)
{
	float movementSpeed = 0.2f;

	//float angle = atan2((player.playerImage.getPosition().y - enemyImage.getGlobalBounds().height / 2) - enemyImage.getPosition().y,
	//	(player.playerImage.getPosition().x - enemyImage.getGlobalBounds().width / 2) - enemyImage.getPosition().x);
	enemyImage.move(cos(enemyImage.getRotation() * 3.14159 / 180)*(movementSpeed), sin(enemyImage.getRotation() * 3.14159 / 180)*(movementSpeed));
	
}