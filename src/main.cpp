// main.cpp
#include "SFML.h"
#include "stdafx.h"
#include <sstream>
#include <cmath>
#include "Projectile.h"
#include "Player.h"
#include "EnemyStinger.h"

int SCREENWIDTH = 1920;
int SCREENHEIGHT = 1080;

enum Direction { Down, Left, Right, Up };

int main(int argc, char** argv)
{
	sf::RenderWindow m_window;

	m_window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Coloussus Core", sf::Style::Fullscreen);

	sf::View view;

	view.reset(sf::FloatRect(0, 0, SCREENWIDTH, SCREENHEIGHT));

	view.setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));

	bool WindowActive(true);
	float angle = 0;
	bool DeleteProjectile = false;
	bool previousleftmouseclick = false;

	sf::Vector2f source(1, Down);

	Player ship(sf::Vector2f(SCREENWIDTH / 100, SCREENHEIGHT / 2));

	std::vector<Projectile*> m_pxProjectile;

	std::vector<EnemyStinger*> m_pxStinger;

	for (int i = 0; i < 3; i++)
	{
		EnemyStinger* pxStinger = new	EnemyStinger(sf::Vector2f(SCREENWIDTH - 20, SCREENHEIGHT / 2), 180);
		m_pxStinger.push_back(pxStinger);
		m_pxStinger[i]->enemyImage.setPosition(sf::Vector2f(SCREENWIDTH - (60 * i * 0.40), SCREENHEIGHT / 2 + (60 * i)));
	}

	sf::Vector2f position(0, 0);

	//sf::Sprite tiles;
	//std::vector < std::vector < sf::Vector2i>> map;
	//std::vector <sf::Vector2i> tempMap;
	//LoadMap("assets/Map1.txt", map, tempMap, tiles);
	//std::vector <std::vector<int>> colMap;
	//std::vector <int> col;
	//LoadColMap("ColMap1.txt", colMap, col);
	sf::Texture cave1;
	sf::Sprite Cave1;
	cave1.loadFromFile("assets/Section1.png");
	Cave1.setTexture(cave1);
	Cave1.setScale(1, 1);
	Cave1.setPosition(-900, -500);

	while (m_window.isOpen())
	{
		sf::Event Event;
		while (m_window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			case sf::Event::GainedFocus:
				std::cout << "Window Active" << std::endl;
				WindowActive = true;
				break;
			case sf::Event::LostFocus:
				std::cout << "Window NOT Active" << std::endl;
				WindowActive = false;
				break;
			case sf::Event::Resized:
				std::cout << "Width : " << Event.size.width << " Height : " << Event.size.height << std::endl;
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
				{
					m_window.close();
				}
				if (Event.key.code == sf::Keyboard::Home)
				{
					m_window.close();
					m_window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Coloussus Core", sf::Style::Fullscreen);
				}
			}
		}
		m_window.clear(sf::Color(40, 40, 40));
		
		m_window.draw(Cave1);

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			if (!previousleftmouseclick)
			{
				sf::Vector2f playerPositionOnScreen = sf::Vector2f(ship.playerImage.getPosition() - (view.getCenter() - view.getSize() * 0.5f));
				Projectile* pxProjectile = new Projectile(sf::Vector2f(ship.playerImage.getPosition().x, ship.playerImage.getPosition().y), atan2f(sf::Mouse::getPosition(m_window).y - playerPositionOnScreen.y, sf::Mouse::getPosition(m_window).x - playerPositionOnScreen.x) / 3.14159 * 180);
				m_pxProjectile.push_back(pxProjectile);
			}
			previousleftmouseclick = true;
		}
		else
			previousleftmouseclick = false;
			
		//for (int i = 0; i < m_pxStinger.size(); i++)
		//{
		//	m_pxStinger[i]->Movement(ship);
		//	if (m_pxStinger[i]->enemyImage.getPosition().x <= ship.playerImage.getPosition().x)
		//	{
		//		m_pxStinger[i]->enemyImage.setScale(-1, 1);
		//		
		//		//m_window.setPosition(sf::Vector2i(100 + rand() % 25, 100 + rand() % 25));
		//	}
		//	else if (m_pxStinger[i]->enemyImage.getPosition().x >= ship.playerImage.getPosition().x)
		//	{
		//		m_pxStinger[i]->enemyImage.setScale(1, 1);
		//	}
		//	for (int j = 0; j < m_pxProjectile.size(); j++)
		//	{
		//		if (m_pxStinger[i]->enemyImage.getPosition().x <= m_pxProjectile[j]->ProjectileImage.getPosition().x)
		//		{

		//		}
		//	}
		//}

		//for (int i = 0; i < colMap.size(); i++)
		//{
		//	for (int j = 0; j < colMap[i].size(); j++)
		//	{
		//		if (colMap[i][j] == 1)
		//		{
		//			int bottom, top, lefddd

		//for (int i = 0; i < map.size(); i++)
		//{
		//	for (int j = 0; j < map[i].size(); j++)
		//	{
		//		if (map[i][j].x != -1 && map[i][j].y != -1)
		//		{
		//			tiles.setPosition(j * 32, i * 80);
		//			tiles.setScale(1, 1);
		//			tiles.setTextureRect(sf::IntRect(map[i][j].x * 32, map[i][j].y * 32, 32, 32));
		//			m_window.draw(tiles);
		//		}
		//	}
		//}
	
				
			
		position.x = ship.playerImage.getPosition().x + 10 - (SCREENWIDTH / 2);
		position.y = ship.playerImage.getPosition().y + 10 - (SCREENHEIGHT / 2);
	
		view.reset(sf::FloatRect(position.x, position.y, SCREENWIDTH, SCREENHEIGHT));
		m_window.setView(view);

		if (WindowActive)
		ship.Movement(ship.playerImage, source); //Source is for Animation sprite
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
		m_window.draw(ship.playerImage); //Draw Player Sprite;

		for (int i = 0; i < m_pxProjectile.size(); i++)
		{
			m_window.draw(m_pxProjectile[i]->ProjectileImage);
			m_pxProjectile[i]->Move();

			sf::FloatRect rect(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2, view.getSize().x, view.getSize().y);
			if (!rect.intersects(m_pxProjectile[i]->ProjectileImage.getGlobalBounds()))
			{
				delete m_pxProjectile[i];
				m_pxProjectile.erase(m_pxProjectile.begin() + i);
				i--;
			}
			if (i == 50)
			{
				delete m_pxProjectile[i];
				m_pxProjectile.erase(m_pxProjectile.begin() + i);
				i--;
			}
		}
	
		for (int i = 0; i < m_pxStinger.size(); i++)
		{
			m_pxStinger[i]->Movement(ship);
			m_window.draw(m_pxStinger[i]->enemyImage);
		}
		m_window.display();
		
	}
}



