// main.cpp
#include "SFML.h"
#include "stdafx.h"
#include <sstream>
#include <cmath>

int SCREENWIDTH = 1280;
int SCREENHEIGHT = 780;
enum Direction { Down, Left, Right, Up };


class Player
{
public:
	sf::Sprite playerImage;
	sf::Texture pTexture;
	float bottom, left, right, top;

	Player(sf::Vector2f p_xPposition)
	{
		playerImage.setPosition(p_xPposition);
		if (!pTexture.loadFromFile("assets/Aquila.png"))
			std::cout << "Error could not load image" << std::endl;
		playerImage.setTexture(pTexture);//305,441
		playerImage.setOrigin(playerImage.getLocalBounds().width / 2, playerImage.getLocalBounds().height / 2);
	}
	void Update()
	{
		bottom = playerImage.getPosition().y + playerImage.getLocalBounds().height;
		left = playerImage.getPosition().x;
		right = playerImage.getPosition().x + playerImage.getLocalBounds().width;
		top = playerImage.getPosition().y;
	}

	bool Collision(Player Collider)
	{
		if (left < Collider.right || right > Collider.left
			|| top > Collider.bottom || bottom < Collider.top)
		{
			return false;
		}
		return true;
	}
	void Movement(sf::Sprite &p_pxPlayer, sf::Vector2f &p_xsource)
	{
		/*PlayerMovement*/ {
			sf::Clock clockmovement;
			clockmovement.restart();
			float moveSpeed = 10000000.0f;
			float RotationSpeed = 0.3f;
			//Convert angle to radians
			float angleRADS;
			angleRADS = (3.1415926536 / 180)*(playerImage.getRotation());

			//Set x and y
			float forx = 0.01f*cos(angleRADS);
			float fory = 0.01f*sin(angleRADS);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				p_pxPlayer.rotate(-RotationSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				p_pxPlayer.rotate(RotationSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				p_xsource.y = Right;
				p_pxPlayer.move(forx  * moveSpeed * clockmovement.getElapsedTime().asSeconds(), fory *  moveSpeed * clockmovement.getElapsedTime().asSeconds());
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				p_xsource.y = Left;
				p_pxPlayer.move(forx * -moveSpeed * clockmovement.getElapsedTime().asSeconds(), fory *  -moveSpeed * clockmovement.getElapsedTime().asSeconds());
			}

			//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q) /*&& sf::Keyboard::isKeyPressed(sf::Keyboard::W) ||
			//	sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::W)*/)
			//{
			//		p_pxPlayer.rotate(-RotationSpeed);
			//}
			//else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)/* && sf::Keyboard::isKeyPressed(sf::Keyboard::S) || 
			//	sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::S)*/)
			//{
			//		p_pxPlayer.rotate(RotationSpeed);
			//}

		}
	}

};

class Enemy
{
public:
	sf::Sprite enemyImage;
	sf::Texture eTexture;
	float ebottom, eleft, eright, etop;

	Enemy(sf::Vector2f p_xEposition)
	{
		enemyImage.setPosition(p_xEposition);
		if (!eTexture.loadFromFile("assets/Player.png"))
			std::cout << "Error could not load image" << std::endl;
		enemyImage.setTexture(eTexture);//305,441
		enemyImage.setOrigin(enemyImage.getLocalBounds().width / 2, enemyImage.getLocalBounds().height / 2);
	}
	void Update()
	{
		ebottom = enemyImage.getPosition().y + enemyImage.getLocalBounds().height;
		eleft = enemyImage.getPosition().x;
		eright = enemyImage.getPosition().x + enemyImage.getLocalBounds().width;
		etop = enemyImage.getPosition().y;
	}

	bool Collision(Player Collider)
	{
		if (eright < Collider.left || eleft > Collider.right
			|| etop > Collider.bottom || ebottom < Collider.top)
		{
			return false;
		}
		return true;
	}

};

class Missile
{
public:
	
	sf::Sprite missileImage;
	sf::Texture mTexture;
	float bottom, left, right, top;
	bool m_bActive = false;
	void Activate()
	{
		m_bActive = true;
	}

	void Deactivate()
	{
		m_bActive = false;
	}

	bool isActive()
	{
		if (m_bActive == true)
		{
			return true;
		}
		return false;
	}
	Missile(sf::Vector2f position)
	{
		missileImage.setPosition(position);
		if (!mTexture.loadFromFile("assets/Missile.png"))
			std::cout << "Error could not load image" << std::endl;
		missileImage.setTexture(mTexture);//305,441
		missileImage.setScale(1.0, 1.0);
		missileImage.setOrigin(missileImage.getLocalBounds().width, 0);
	}

	void Move(Player &player)
	{
		//Convert angle to radians
		float movespeed = 100;
		float angleRADS;
		angleRADS = (3.1415926536 / 180)*(player.playerImage.getRotation());

		//Set x and y
		float forx = 0.01f*cos(angleRADS);
		float fory = 0.01f*sin(angleRADS);

		missileImage.move(forx * movespeed, fory * movespeed);
		/*missileImage.setRotation(forx);*/

	}

	void Update()
	{
		bottom = missileImage.getPosition().y + missileImage.getLocalBounds().height;
		left = missileImage.getPosition().x;
		right = missileImage.getPosition().x + missileImage.getLocalBounds().width;
		top = missileImage.getPosition().y;
	}

	bool Collision(Missile Collider)
	{
		if (right < Collider.left || left > Collider.right
			|| top > Collider.bottom || bottom < Collider.top)
		{
			return false;
		}
		return true;
	}

};

void LoadMap(char* filename, std::vector<std::vector<sf::Vector2i> > &map, std::vector<sf::Vector2i> &tempMap, sf::Sprite &tiles)
{
	/*TileSystem*/
	std::ifstream openfile(filename);
	sf::Texture tileTexture;
	

	if (openfile.is_open())
	{
		std::string tileLocation;
		openfile >> tileLocation;
		tileTexture.loadFromFile(tileLocation);
		tiles.setTexture(tileTexture);
		while (!openfile.eof())
		{

			std::string str, value;
			std::getline(openfile, str);
			std::stringstream stream(str);

			while (std::getline(stream, value, ' '))
			{
				if (value.length() > 0)
				{
					std::string xx = value.substr(0, value.find(','));
					std::string yy = value.substr(value.find(',') + 1);

					int x, y, i, j;
					for (i = 0; i < xx.length(); i++)
					{
						if (!isdigit(xx[i]))
							break;
					}
					for (j = 0; j < xx.length(); j++)
					{
						if (!isdigit(xx[j]))
							break;
					}
					x = (j == xx.length()) ? atoi(xx.c_str()) : -1;
					y = (j == yy.length()) ? atoi(yy.c_str()) : -1;

					tempMap.push_back(sf::Vector2i(x, y));
				}
			}
			map.push_back(tempMap);
			tempMap.clear();
		}
	}
}

sf::Vector2f calculation(sf::RenderWindow &window, Player &player)
{
	float eX;
	float O;
	float calc;
	float x = sf::Mouse::getPosition(window).x - player.playerImage.getPosition().x;
	float y = sf::Mouse::getPosition(window).y - player.playerImage.getPosition().y;

	O = std::tan(y / x);


	O = O * (180 / 3.1415926536);

	std::cout << O << std::endl;
	return sf::Vector2f (0,0);
}


int main(int argc, char** argv)
{
	/*Commented code might be useful*/ {
		/*case sf::Event::TextEntered:
		if(Event.text.unicode >= 33 && Event.text.unicode <= 126)
		sentence += (char)Event.text.unicode;
		else if (Event.text.unicode == 8 && sentence.getSize() > 0)
		sentence.erase(sentence.getSize() - 1, sentence.getSize());

		//bool FireMissile(false);

		text.setString(sentence);
		break;*/
		//for (int i = 0; i < MissileAmmo; i++)
		//{
		//	Missile* pxMissile = new Missile(sf::Vector2f(ship.playerImage.getPosition().x + 50, ship.playerImage.getPosition().y + 50));
		//	m_pxMissile.push_back(pxMissile);
		//}
		//		if (!FireMissile)
		//		{

		//			m_pxMissile->missileImage.setPosition(ship.playerImage.getPosition().x + 50, ship.playerImage.getPosition().y + 50);
		//			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		//			{
		//				FireMissile = true;
		//			}
		//		}
		//		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		//		{
		//			FireMissile = false;

		//		}
		//		if (FireMissile)
		//		{
		//			m_pxMissile->
		//		}
		//		m_window.draw(m_pxMissile->missileImage);
		//	}
		//if (m_pxMissile )
		/*source.x++;
		if (source.x * 32 >= pTexture.getSize().x)
		source.x = 0;*/
		
		//ship.playerImage.setTextureRect(sf::IntRect(source.x * 32, source.y * 32, 32,32)); //Player Animation
	}
	sf::RenderWindow m_window;
	m_window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Coloussus Core", sf::Style::Titlebar | sf::Style::Close);
	sf::View view;
	view.reset(sf::FloatRect(0, 0, SCREENWIDTH, SCREENHEIGHT));
	view.setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));
	bool WindowActive(true);
	float angle = 0;
	sf::Vector2f source(1, Down);
	Player ship(sf::Vector2f(SCREENWIDTH / 100, SCREENHEIGHT / 2));
	Player ship2(sf::Vector2f(100, 100));
	ship2.playerImage.setPosition(100, 100);
	std::vector<Missile*> m_pxMissile;

	Enemy Stinger(sf::Vector2f(SCREENWIDTH - 20, SCREENHEIGHT / 2));
	sf::Vector2f pointB;
	
	sf::Vector2f position(0, 0);
	for (int i = 0; i <= 20; i++)
	{
		Missile* pxMissile = new Missile(sf::Vector2f(ship.playerImage.getPosition().x, ship.playerImage.getPosition().y));
		m_pxMissile.push_back(pxMissile);
		
	}
	
	bool FireMissile(false);
	int shotfired = 0;


	
	

	sf::Sprite tiles;
	std::vector < std::vector < sf::Vector2i>> map;
	std::vector <sf::Vector2i> tempMap;
	LoadMap("assets/Map1.txt", map, tempMap, tiles);



	while (m_window.isOpen())
	{
		sf::Event Event;
		while (m_window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			case sf::Event::GainedFocus:
				std::cout << "Window Active" << std::endl;
				WindowActive = true;
				break;
			case sf::Event::LostFocus:
				std::cout << "Window NOT Active" << std::endl;
				WindowActive = false;
				break;
			case sf::Event::Resized:
				std::cout << "Width : " << Event.size.width << " Height : " << Event.size.height << std::endl;
				break;
			case sf::Event::KeyPressed:
				if (Event.key.code == sf::Keyboard::Escape)
				{
					m_window.close();
				}

			}
		}
		m_window.clear(sf::Color(40, 40, 40));


		//std::cout << ship.playerImage.getPosition().x << std::endl;
		//std::cout << newpos.x << " " << newpos.y << std::endl;

		if (!FireMissile)
		{
			for (int i = 0; i < m_pxMissile.size(); i++)
			{
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					FireMissile = true;
					angle = atan2(sf::Mouse::getPosition(m_window).y - ship.playerImage.getPosition().y, sf::Mouse::getPosition(m_window).x - ship.playerImage.getPosition().x);
					angle = angle * 180 / (atan(1) * 4);
					shotfired++;
				}
				m_pxMissile[i]->missileImage.setPosition(ship.playerImage.getPosition().x, ship.playerImage.getPosition().y);
			}
		}
		sf::Vector2f newpos((cos(angle)) * 2, (sin(angle)) * 2);
		if (FireMissile)
		{
			for (int i = 0; i < m_pxMissile.size(); i++)
			{
				//if(m_pxMissile[i]->isActive())
				m_window.draw(m_pxMissile[i]->missileImage);
				m_pxMissile[i]->Move(ship);
				//m_pxMissile[i]->missileImage.setPosition(sf::Mouse::getPosition(m_window).x, sf::Mouse::getPosition(m_window).y);
			}
		}

		for (int i = 0; i < m_pxMissile.size(); i++)
		{
			if (m_pxMissile[i]->missileImage.getPosition().x > view.getSize().x)
			{
				FireMissile = false;
				//m_pxMissile[i]->Deactivate();
				/*m_pxMissile[i] = nullptr;
				delete m_pxMissile[i];*/
			}
		}


		for (int i = 0; i < map.size(); i++)
		{
			for (int j = 0; j < map[i].size(); j++)
			{
				if (map[i][j].x != -1 && map[i][j].y != -1)
				{
					tiles.setPosition(j * 32, i * 80);
					tiles.setScale(1, 1);
					tiles.setTextureRect(sf::IntRect(map[i][j].x * 32, map[i][j].y * 32, 32, 32));
					m_window.draw(tiles);
				}
			}
		}

	
	//	calculation(m_window, ship);
		if (ship.Collision(ship2))
		{
		//	std::cout << "Collision" << std::endl;
		}
		position.x = ship.playerImage.getPosition().x + 10 - (SCREENWIDTH / 2);
		position.y = ship.playerImage.getPosition().y + 10 - (SCREENHEIGHT / 2);
		if (position.x < 0)
			position.x = 0;
		if (position.y < 0)
			position.y = 0;
		view.reset(sf::FloatRect(position.x, position.y, SCREENWIDTH, SCREENHEIGHT));
		m_window.setView(view);

		if (WindowActive)
		ship.Movement(ship.playerImage, source); //Source is for Animation sprite
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
		m_window.draw(ship.playerImage); //Draw Player Sprite;
		m_window.draw(Stinger.enemyImage);
		m_window.display();
		
	}
}



